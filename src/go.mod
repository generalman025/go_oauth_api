module gitlab.com/generalman025/go_oauth_api

go 1.15

require (
	github.com/generalman025/template_go_util_lib_api v0.0.0-20210228122926-f18a15774ad1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-resty/resty/v2 v2.5.0
	github.com/gocql/gocql v0.0.0-20210129204804-4364a4b9cfdd
	github.com/stretchr/testify v1.7.0
)
