package main

import (
	"gitlab.com/generalman025/go_oauth_api/app"
)

func main() {
	app.StartApplication()
}
